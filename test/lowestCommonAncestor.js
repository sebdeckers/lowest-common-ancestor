import test from 'ava'
import {lowestCommonAncestor} from '..'

const fixtures = [
  {
    label: 'Files in the same directory',
    given: [
      '/foo/bar/abc/def.txt',
      '/foo/bar/abc/ghi.txt'
    ],
    expected: '/foo/bar/abc'
  },
  {
    label: 'Identical paths',
    given: [
      '/foo/bar/abc/def',
      '/foo/bar/abc/def'
    ],
    expected: '/foo/bar/abc/def'
  },
  {
    label: 'Multiple levels up',
    given: [
      '/bar/foo/abc/def',
      '/foo/bar/abc/def'
    ],
    expected: '/'
  },
  {
    label: 'More than two paths',
    given: [
      '/foo/bar/abc/def',
      '/foo/bar/abc/ghi',
      '/foo/bar/abc',
      '/foo/bar'
    ],
    expected: '/foo/bar'
  },
  {
    label: 'Reverse depths',
    given: [
      '/foo/bar',
      '/foo/bar/abc',
      '/foo/bar/abc/def',
      '/foo/bar/abc/ghi'
    ],
    expected: '/foo/bar'
  },
  {
    label: 'Mixed depths',
    given: [
      '/foo/bar/abc',
      '/foo/bar',
      '/foo/bar/abc/def',
      '/foo',
      '/foo/bar/abc/ghi'
    ],
    expected: '/foo'
  },
  {
    label: 'Relative paths',
    given: [
      './foo/bar/abc',
      './foo/bar',
      './foo/bar/abc/def',
      './foo',
      './foo/bar/abc/ghi'
    ],
    expected: './foo'
  },
  {
    label: 'Relative paths without current directory',
    given: [
      'foo/bar/abc',
      'foo/bar',
      'foo/bar/abc/def',
      'foo',
      'foo/bar/abc/ghi'
    ],
    expected: 'foo'
  },
  {
    label: 'No input paths',
    given: [],
    expected: ''
  },
  {
    label: 'Single relative path',
    given: ['./foo'],
    expected: ''
  },
  {
    label: 'Single absolute path',
    given: ['/foo'],
    expected: ''
  },
  {
    label: 'Single relative path without current directory',
    given: ['foo'],
    expected: ''
  }
]

for (const {label, given, expected} of fixtures) {
  test(label, (t) => {
    const actual = lowestCommonAncestor(...given)
    t.is(actual, expected)
  })
}
