# lowest-common-ancestor

## `lowestCommonAncestor(...filepaths)`

Finds the deepest shared file path.

The OS-dependent directory separator (e.g. `/` or `\`) is respected.

### `filepaths`

Multiple relative or absolute file system paths.

## Usage

```js
import {lowestCommonAncestor} from 'lowest-common-ancestor'
```

```js
lowestCommonAncestor(
  '/foo/bar/abc',
  '/foo/bar/def',
  '/foo/xyz'
)

// -> '/foo'
```

```js
const paths = new Set([
  '/foo/bar/abc',
  '/foo/bar/def',
  '/foo/xyz'
])

lowestCommonAncestor(...paths)

// -> '/foo'
```
